import DS from 'ember-data';
import config from '../config/environment';

export default DS.RESTAdapter.extend({
	host: config.adapterURL,
	namespace: 'becas/v2'
});



import DS from 'ember-data';

export default DS.Model.extend({
	nombre: DS.attr('string'),
	fecha: DS.attr('string'),
	periodo: DS.belongsTo('periodo', {async: true}),
	registros: DS.hasMany('registro', {async: true})
  
});

import DS from 'ember-data';

export default DS.Model.extend({
	mail: DS.attr('string'),
	nombre: DS.attr('string'),
	extension: DS.attr('number'),

	registros: DS.hasMany('registro', {async: true})
  
});

import DS from 'ember-data';

export default DS.Model.extend({
	semestre: DS.attr('string'),
  	parciales: DS.hasMany('parcial', {async: true})
  
});

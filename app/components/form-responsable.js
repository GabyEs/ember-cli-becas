import Ember from 'ember';

export default Ember.Component.extend({
	responsable: null,

	actions:{
		saveResponsable:function(){
			this.get('responsable').save().then(function(){
				alert('responsable guardado');
				this.sendAction('didSave');
			}.bind(this));
		}
	}
});
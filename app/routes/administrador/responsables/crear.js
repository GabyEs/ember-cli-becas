import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		// regresar el modelo de la ruta
		return this.store.createRecord('responsable');
	},

	actions:{
		componentDidSave:function(){
			this.transitionTo('administrador.responsables');
		}
	}
});

import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		return this.store.findAll('responsable'); //regresa un arreglo especial de ember	
	},

	actions: {
		eliminarResponsable: function(responsable){
			responsable.destroyRecord();

		}
	}
});

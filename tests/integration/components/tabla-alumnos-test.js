import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('tabla-alumnos', 'Integration | Component | tabla alumnos', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{tabla-alumnos}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#tabla-alumnos}}
      template block text
    {{/tabla-alumnos}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

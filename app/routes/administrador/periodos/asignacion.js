import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		return this.store.createRecord('registro');
	},
	actions: {
		pideSelects: function(){
			// las peticiones para traer el contenido de los select
			this.store.find('alumno').then(function(alumnos){
				this.controller.set('alumnos', alumnos);
			}.bind(this));

			this.store.find('responsable').then(function(responsables){
				this.controller.set('responsables', responsables);
			}.bind(this));

				this.store.find('periodo').then(function(periodos){
				this.controller.set('periodos', periodos);
			}.bind(this));
		},
		//FALTA HACER EL SAVE DEL REGISTRO
		didRegistro: function(selectedAlumno, selectedResponsable, selectedPeriodo){
			var model = this.modelFor('administrador.periodos.asignacion');
			model.setProperties({
				alumno: selectedAlumno,
				responsable: selectedResponsable,
				periodo: selectedPeriodo
			});

			model.save().then(function(registro){
				alert("YEI!");
				console.log(registro);
			}, function(cause){
				alert("NO!");
				console.log(cause);
			})
		}
	}
});


import Ember from 'ember';

export default Ember.Component.extend({
	alumnos: null,

	actions: {
		eliminarAlumno: function(alumno){
			this.sendAction('eliminarAlumno',alumno);

		}
	}
});

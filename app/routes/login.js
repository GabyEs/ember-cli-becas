import Ember from 'ember';

export default Ember.Route.extend({
	actions:{
		doLogin: function(email, password){

			/* Consulta al "servidor" para loguear usuario 
			var users = this.store.peekAll('user');
			var user = users.find(function(u){
				return u.get('email') === email;
			}.bind(this));
			*/

		    Ember.$.post('http://labweb.noblyn.com:4200/becas/v2/users/login',{
			 	e: email,
			 	p: password
			 }).done(function(obj){
			 	localStorage.authToken = obj.token;
			 	debugger;

			 	switch(obj.user_type){
					case 1:
						this.transitionTo('administrador');
						break;
					case 2:
						this.transitionTo('responsable',{responsable_id: obj.owner_id});
						break;
					case 3:
						this.transitionTo('alumno', {alumno_id: obj.owner_id});
						break;
					default:
						alert("No tiene permisos este usuario");
						break;
				}

			 }.bind(this)
			 ).fail(function(response){
			 	debugger;
			 	alert(response.responseJSON.errors);
			 }.bind());


		}
	}
});

import Ember from 'ember';

export default Ember.Component.extend({
	responsables:null,
	alumnos: null,
	alumnosFiltrados: [],
	periodos: null,
	registros: null,
	didConsulta: '',
	selectedPeriodo: null,
	selectedResponsable: null,
	selectedCarrera: null,
	horas: null,

	llenaSelects:function(){
		this.sendAction('pideSelects');
	}.on('didInsertElement'),

	actions: {
		doConsulta: function(){
			var selectedPeriodo = this.get('selectedPeriodo');
			var selectedResponsable = this.get('selectedResponsable');
			var selectedCarrera = this.get('selectedCarrera');
			var horas = this.get('horas');
			this.sendAction('didConsulta', selectedPeriodo, selectedResponsable, selectedCarrera, horas);
		}

	}

});

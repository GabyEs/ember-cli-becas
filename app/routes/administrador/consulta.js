import Ember from 'ember';
const { isEmpty } = Ember;

export default Ember.Route.extend({
	beforeModel: function(){
		//this.store.findAll('parcial');
	},
	
	actions: {
		pideSelects: function(){
			this.store.find('responsable').then(function(responsables){
				this.controller.set('responsables', responsables);
			}.bind(this));

			this.store.find('periodo').then(function(periodos){
				this.controller.set('periodos', periodos);
			}.bind(this));

			this.store.find('alumno').then(function(alumnos){
				this.controller.set('alumnos', alumnos);
			}.bind(this));

		},

		didConsulta: function(selectedPeriodo, selectedResponsable, selectedCarrera, horas){
			let params = {}, controller = this.controller;

			if (!isEmpty(selectedPeriodo)){
				params.semestre = selectedPeriodo.get('semestre');
			}

			if(!isEmpty(selectedResponsable)){
				params.responsable = selectedResponsable.get('nombre');
			}

			if(!isEmpty(selectedCarrera)){
				params.carrera = selectedCarrera.get('carrera');
			}

			if(!isEmpty(horas)){
				params.horas = horas;
			}

			Ember.$.ajax({
				url: '/becas/v2/busquedas',
				data: params
			}).then(function(data){
				controller.set('alumnosFiltrados', data.alumnos);
			})

			// Ember.$.ajax({
			// 	url: '/becas/v2/busca_parciales',
			// 	data: params
			// }).then(function(data){
			// 	controller.set('parciales', data.parciales);

			// 	return Ember.$.ajax({
			// 		url: '/becas/v2/busquedas',
			// 		data: params
			// 	}).then(function(data){
			// 		controller.set('alumnosFiltrados', data.alumnos);
			// 	})
			// }, function(cause){
			// 	alert(cause);
			// });
		}
	}


});

import DS from 'ember-data';

export default DS.Model.extend({
	autorizaAlum: DS.attr('boolean'),
	autorizaRes: DS.attr('boolean'),
	horas: DS.attr('number'),
	comentario: DS.attr('string'),
	calificacion: DS.attr('number'),

	alumno: DS.belongsTo('alumno'),
	responsable: DS.belongsTo('responsable', {async: true}),
	parcial: DS.belongsTo('parcial', {async: true})
  
});



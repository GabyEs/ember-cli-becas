import Ember from 'ember';

export default Ember.Route.extend({
	actions:{
		buscar: function (periodo, matricula) {
			this.store.query('alumno',{periodo:periodo, matricula:matricula}).then(function(response){
				this.controller.set('model', response);

			}.bind(this))
		},
		eliminarAlumno: function(alumno){
			alumno.destroyRecord();

		}
	}
});
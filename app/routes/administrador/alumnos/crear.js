import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		// regresar el modelo de la ruta
		return this.store.createRecord('alumno');
	},

	actions:{
		componentDidSave:function(){
			this.transitionTo('administrador.alumnos');
		}
	}


});

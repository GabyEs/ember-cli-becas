import Ember from 'ember';

export default Ember.Component.extend({
	numParciales: null,

	numParcialesCambio: function(){
		let numIntroducido = Number(this.get('numParciales'));
		if(!numIntroducido || numIntroducido < 0) { return; }

		let numParcialesDelPeriodo =  this.get('periodo.parciales.length');

		      // 3                4
		if(numIntroducido < numParcialesDelPeriodo){
			//                                       4                       3
			for(let i = numParcialesDelPeriodo; this.get('periodo.parciales.length') > numIntroducido; i++){
				this.get('periodo.parciales.lastObject').destroyRecord();
			}
		}
		

		for(let i = numParcialesDelPeriodo; i < numIntroducido; i++){
			this.get('periodo.parciales').createRecord({
				nombre: 'Parcial ' + (i + 1)
			});
		}

	}.observes('numParciales'),

	actions: {
		savePeriodo: function(){
			this.get('periodo').save().then(function(){
				this.get('periodo.parciales').invoke('save');
				alert('periodo guardado');
			}.bind(this));
		}
	}
});

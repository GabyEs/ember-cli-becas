import Ember from 'ember';

export default Ember.Component.extend({
	responsables:null,
	alumnos:null,
	didRegistro: '',
	selectedAlumno: null,
	selectedResponsable: null,
	selectedPeriodo: null,

	llenaSelects:function(){
		this.sendAction('pideSelects');
	}.on('didInsertElement'),


	actions: {
		saveAsignacion: function(){
			var selectedAlumno = this.get('selectedAlumno');
			var selectedResponsable = this.get('selectedResponsable');
			var selectedPeriodo = this.get('selectedPeriodo');
			this.sendAction('didRegistro', selectedAlumno, selectedResponsable, selectedPeriodo);
		}

	}

});



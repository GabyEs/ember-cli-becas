import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){

		return this.store.findAll('periodo'); //regresa un arreglo especial de ember
		
	},

	actions: {
		eliminarPeriodo: function(periodo){
			periodo.destroyRecord();

		}
	}

});

import Ember from 'ember';

export default Ember.Component.extend({
	alumno: null,

	actions:{
		saveAlumno:function(){
			this.get('alumno').save().then(function(){
				alert('alumno guardado');
				this.sendAction('didSave');
			}.bind(this));
		}
	}
});
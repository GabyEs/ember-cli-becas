import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('login', {path:'/'});
  
  this.route('alumno', {path: ':alumno_id/alumno'});
  
  this.route('responsable', {path: ':responsable_id/responsable'}, function() {
    this.route('autorizar', {path: ':alumno_id/autorizar'});
  });
  
  this.route('administrador', {}, function(){
  	this.route('alumnos', {}, function(){
        this.route('crear');
        this.route('editar', {path: ':alumno_id/editar'});
        this.route('checar',{path: ':alumno_id/checar'});
  	});

    this.route('responsables', {}, function(){
        this.route('crear');
        this.route('editar',{path: ':responsable_id/editar'});
    });

  	//this.route('asignacion');
    
    this.route('consulta');


    this.route('periodos', {}, function(){
        this.route('crear');
        this.route('editar', {path: ':periodo_id/editar'});
        this.route('asignacion');
    });

  });
});

export default Router;

import Ember from 'ember';

export default Ember.Component.extend({
	periodo:null,
	matricula:null,

	actions:{
		buscar: function(){
			this.sendAction('buscarEnRuta', this.get('periodo'),this.get('matricula'));

		}
	}

});

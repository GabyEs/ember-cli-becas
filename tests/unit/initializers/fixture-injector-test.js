import Ember from 'ember';
import FixtureInjectorInitializer from '../../../initializers/fixture-injector';
import { module, test } from 'qunit';

let application;

module('Unit | Initializer | fixture injector', {
  beforeEach() {
    Ember.run(function() {
      application = Ember.Application.create();
      application.deferReadiness();
    });
  }
});

// Replace this with your real tests.
test('it works', function(assert) {
  FixtureInjectorInitializer.initialize(application);

  // you would normally confirm the results of the initializer here
  assert.ok(true);
});

import DS from 'ember-data';

export default DS.Model.extend({
	nombre: DS.attr('string'),
	carrera: DS.attr('string'),
	matricula: DS.attr('string'),
	semestre: DS.attr('number'),

	registros: DS.hasMany('registro', {async: true})
});

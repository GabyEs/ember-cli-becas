import Ember from 'ember';

export default Ember.Route.extend({
	model: function(params){
		return this.store.find('alumno', params.alumno_id);
	}
});

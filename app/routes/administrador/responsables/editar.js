import Ember from 'ember';

export default Ember.Route.extend({
	actions:{
		componentDidSave:function(){
			this.transitionTo('administrador.responsables');
		}
	}
});
